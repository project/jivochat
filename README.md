# JivoChat

A simple module to append the script into header for [JivoChat](#1-jivosite)
chat widget.

## INTRODUCTION

Nothing much ... just use it

## REQUIREMENTS

Drupal 8 / 9

## INSTALLATION

Download a tar or zip, extract into `modules/contrib` folder, then enable 
the module.
Or, do it with composer: `$ composer require drupal/jivochat`

## CONFIGURATION

Navigato to Admin > Config > Web Services > JivoChat for configurations.

[1-jivosite]: https://www.jivosite.com

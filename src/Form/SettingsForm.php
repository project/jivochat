<?php

namespace Drupal\jivochat\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'jivochat.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jivochat_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('jivochat.settings');

    $form['widget_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Widget ID'),
      '#description' => $this->t('JivoChat Widget ID which is given upon creation of the widget'),
      '#maxlength' => 20,
      '#size' => 30,
      '#default_value' => $config->get('widget_id'),
      '#required' => TRUE,
    ];

    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('HOST server override'),
      '#description' => $this->t('JivoChat Widget can be placed on many different servers'),
      '#maxlength' => 120,
      '#size' => 120,
      '#default_value' => $config->get('host'),
    ];

    $form['admin_show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show on admin pages'),
      '#description' => $this->t("It's a nonsense to show JIVO on admin pages, but if you need it - check this one"),
      '#default_value' => $config->get('admin_show'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('jivochat.settings')
      ->set('host', $form_state->getValue('host'))
      ->set('widget_id', $form_state->getValue('widget_id'))
      ->set('admin_show', $form_state->getValue('admin_show'))
      ->save();
  }

}
